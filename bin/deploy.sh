#!/usr/bin/env sh
set -e
set -v

DOCKER_COMPOSE_FILE=docker-compose.yml
DOCKER_CERT_PATH=/root/.docker

docker info
docker-compose version

mkdir $DOCKER_CERT_PATH
echo "$CA_PEM" | tr -d '\r' > $DOCKER_CERT_PATH/ca.pem
echo "$CERT_PEM" | tr -d '\r' > $DOCKER_CERT_PATH/cert.pem
echo "$KEY_PEM" | tr -d '\r' > $DOCKER_CERT_PATH/key.pem
chmod 400 $DOCKER_CERT_PATH/ca.pem
chmod 400 $DOCKER_CERT_PATH/cert.pem
chmod 400 $DOCKER_CERT_PATH/key.pem

export DOCKER_TLS_VERIFY=1
export DOCKER_HOST=tcp://$DEPLOY_HOST:2376

docker-compose \
  -f $DOCKER_COMPOSE_FILE \
  ps

docker-compose \
  -f $DOCKER_COMPOSE_FILE \
  build robbie-bot

docker-compose \
  -f $DOCKER_COMPOSE_FILE \
  up -d --force-recreate
