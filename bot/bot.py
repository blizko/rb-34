import requests
import os
import re
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.view import view_config, view_defaults
from pyramid.response import Response
from github import Github, GithubException

import logging
import sys

log = logging.getLogger()
log.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

NOTIFICATION_URL = 'http://twinkle.railsc.ru/messages?token='+os.environ['TWINKLE_TOKEN']+'&channel='+os.environ['NOTIFICATION_CHANNEL']
CONFLICTS_NOTIFICATION_URL = 'http://twinkle.railsc.ru/messages?token='+os.environ['TWINKLE_TOKEN']+'&channel='+os.environ['CONFLICTS_NOTIFICATION_CHANNEL']
ENDPOINT = "webhook"
BRANCH_MASTER = 'master'

RELEASE_NOTIFY = "Мастер на Blizko обновлен"
MERGE_NOTIFY = "Результаты подливания мастера в ветки:\n\n"

SUCCESS_MERGE_MESSAGE = "✅ {branch_name}\n"
CONFLICT_MERGE_MESSAGE = "🚫 {branch_name}\n"

RELEASE_PR_RE = r'^Merge pull request #(\d+)'

OWNER = os.environ['GITHUB_OWNER']
REPO_NAME = os.environ['GITHUB_REPO']
AUTH_TOKEN = os.environ['GITHUB_TOKEN']

@view_defaults(
    route_name=ENDPOINT, renderer="json", request_method="POST"
)
class PayloadView(object):
    """
    View receiving of Github payload. By default, this view it's fired only if
    the request is json and method POST.
    """

    def __init__(self, request):
        self.request = request
        # Payload from Github, it's a dict
        self.payload = self.request.json

    @view_config(header="X-Github-Event:push")
    def payload_push(self):
        """This method is a continuation of PayloadView process, triggered if
        header HTTP-X-Github-Event type is Push"""

        if self.payload['ref'] == "refs/heads/master":
            try:
                m = re.search(RELEASE_PR_RE, self.payload['head_commit']['message'])

                if m == None:
                    link = self.payload['compare']
                else:
                    link = 'https://github.com/'+OWNER+'/'+REPO_NAME+'/pull/'+m.group(1)

                """Уведомление о сливе в мастер"""
                release_msg = RELEASE_NOTIFY+': '+link
                """Уведомим о необходимости выпуска версий гемов"""

                requests.post(NOTIFICATION_URL, data = {'message':release_msg})
                """Запустим процесс подливания мастера в остальные ветки"""
                g = Github(AUTH_TOKEN)
                repo = g.get_repo("{owner}/{repo_name}".format(owner=OWNER, repo_name=REPO_NAME))
                notify = MERGE_NOTIFY
                for b in repo.get_branches():
                    if b.name == BRANCH_MASTER or b.protected:
                        continue

                    try:
                        repo.merge(head=BRANCH_MASTER, base=b.name, commit_message="Merge master into {branch_name} [ci skip]".format(branch_name=b.name))
                        notify += SUCCESS_MERGE_MESSAGE.format(branch_name=b.name)
                    except GithubException:
                        notify += CONFLICT_MERGE_MESSAGE.format(branch_name=b.name)

                requests.post(CONFLICTS_NOTIFICATION_URL, data = {'message':notify})
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise

        return Response("success")

    @view_config(header="X-Github-Event:pull_request")
    def payload_pull_request(self):
        """This method is a continuation of PayloadView process, triggered if
        header HTTP-X-Github-Event type is Pull Request"""

        return Response("success")

    @view_config(header="X-Github-Event:ping")
    def payload_else(self):
        print("Pinged! Webhook created with id {}!".format(self.payload["hook"]["id"]))
        return {"status": 200}


def create_webhook():
    """ Creates a webhook for the specified repository.

    This is a programmatic approach to creating webhooks with PyGithub's API. If you wish, this can be done
    manually at your repository's page on Github in the "Settings" section. There is a option there to work with
    and configure Webhooks.
    """
    EVENTS = ["push", "pull_request"]
    HOST = os.environ['VIRTUAL_HOST']

    config = {
        "url": "https://{host}/{endpoint}".format(host=HOST, endpoint=ENDPOINT),
        "content_type": "json"
    }

    g = Github(AUTH_TOKEN)
    repo = g.get_repo("{owner}/{repo_name}".format(owner=OWNER, repo_name=REPO_NAME))
    repo.create_hook("web", config, EVENTS, active=True)

if __name__ == "__main__":
    config = Configurator()

    # create_webhook()

    config.add_route(ENDPOINT, "/{}".format(ENDPOINT))
    config.scan()

    app = config.make_wsgi_app()
    server = make_server("0.0.0.0", 80, app)
    server.serve_forever()
